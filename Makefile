CC=gcc
AS=nasm
CFLAGS=-Wall -Wextra -g
ASFLAGS=-f elf64

SRC_DIR=src
OBJ_DIR=obj
BIN_DIR=bin

SRC=$(wildcard $(SRC_DIR)/*.c $(SRC_DIR)/*.asm)
OBJ=$(patsubst $(SRC_DIR)/%.c,$(OBJ_DIR)/%.o,$(patsubst $(SRC_DIR)/%.asm,$(OBJ_DIR)/%.o,$(SRC)))
BIN=$(BIN_DIR)/program

.PHONY: all clean

all: clean $(BIN)

clean:
	rm -f $(OBJ) $(BIN)

create_dir:
	mkdir -p $(BIN_DIR)
	mkdir -p $(OBJ_DIR)

$(BIN): $(OBJ)
	mkdir -p $(BIN_DIR)
	$(CC) $(CFLAGS) $^ -o $@ -lm

$(OBJ_DIR)/%.o: $(SRC_DIR)/%.c
	mkdir -p $(OBJ_DIR)
	$(CC) $(CFLAGS) -c $< -o $@ -lm

$(OBJ_DIR)/%.o: $(SRC_DIR)/%.asm
	mkdir -p $(OBJ_DIR)
	$(AS) $(ASFLAGS) $< -o $@

