#pragma once
#include "image_format.h"

struct image apply_sepia_filter(const struct image *image);