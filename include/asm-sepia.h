#pragma once
#include "image_format.h"

struct image apply_asm_sepia(const struct image *image);