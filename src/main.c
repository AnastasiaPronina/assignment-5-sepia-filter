#include <stdio.h>
#include "../include/sepia.h"
#include "../include/image_format.h"
#include "../include/bmp_file_processing.h"
#include "../include/asm-sepia.h"
#include <time.h>

FILE *log_file;

void activate_log(){
    log_file = fopen("log.txt", "a");
    time_t currentTime;
    struct tm *localTime;
    char date[11];

    currentTime = time(NULL);
    localTime = localtime(&currentTime);
    strftime(date, sizeof(date), "%Y-%m-%d", localTime);

    fprintf(log_file, "Дата: %s\n", date);
}

void close_log(){
    fprintf(log_file, "\n");
    fclose(log_file);
}

struct image asm_test(const struct image *img, size_t attempts){
    clock_t start, end;
    struct image r;
    start = clock();
    for (size_t i = 0; i < attempts; i++){
        r = apply_asm_sepia(img);
        free_image_data(&r);
    }
    end = clock();
    double time_used = ((double) (end - start)) / CLOCKS_PER_SEC;
    printf("Time for assembly: %f seconds\n", time_used);
    fprintf(log_file, "Iterations: %u\n", attempts);
    fprintf(log_file, "Time for assembly: %f seconds\n", time_used);
    struct image res = apply_asm_sepia(img);
    return res;
}

struct image c_test(struct image *img, size_t attempts){
    clock_t start, end;
    struct image r;
    start = clock();
    for (size_t i = 0; i < attempts; i++){
        r = apply_sepia_filter(img);
        free_image_data(&r);
    }
    end = clock();
    double time_used = ((double) (end - start)) / CLOCKS_PER_SEC;
    fprintf(log_file, "Iterations: %u\n", attempts);
    printf("Time for C: %f seconds\n", time_used);
    fprintf(log_file, "Time for C: %f seconds\n", time_used);
    struct image res = apply_sepia_filter(img);
    return res;
}

//Передаем путь ко входному файлу, к выходному файлу для ассемблера и к выходному файлу для чистого С
int main(int argc, char *argv[]){
    if (argc != 4){
        return;
    }
    activate_log();
    FILE *f = fopen(argv[1], "rb");
    struct image img;
    from_bmp(f, &img);
    fclose(f);
    struct image asm_res = asm_test(&img, 10000);
    struct image c_res = c_test(&img, 10000);
    f = fopen(argv[2], "wb");
    to_bmp(f, &asm_res);
    fclose(f);
    f = fopen(argv[3], "wb");
    to_bmp(f, &asm_res);
    fclose(f);
    free_image_data(&asm_res);
    free_image_data(&c_res);
    close_log();

}
