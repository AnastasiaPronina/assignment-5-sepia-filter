#include "../include/sepia.h"
#include "stdlib.h"
#include <stdio.h>

static const float sepia_matrix[3][3] = {
    {0.272f, 0.543f, 0.131f},
    {0.349f, 0.686f, 0.168f},
    {0.393f, 0.769f, 0.189f}
};

static uint8_t cut(uint64_t n){
    return n < 256 ? n : 255;
}

static struct pixel sepia_per_pixel(struct pixel *pixel){
    struct pixel new;
    new.r = cut(pixel->r * sepia_matrix[0][0] + pixel->g * sepia_matrix[0][1] + pixel->b * sepia_matrix[0][2]);
    new.g = cut(pixel->r * sepia_matrix[1][0] + pixel->g * sepia_matrix[1][1] + pixel->b * sepia_matrix[1][2]);
    new.b = cut(pixel->r * sepia_matrix[2][0] + pixel->g * sepia_matrix[2][1] + pixel->b * sepia_matrix[2][2]);
    return new;
}

struct image apply_sepia_filter(const struct image *image){
    struct image res = {
        .height = image->height, 
        .width = image->width, 
        .data = NULL};
    create_image_data(&res);
    for (size_t i = 0; i < image->height; i++){
        for (size_t j = 0; j < image->width; j++){
            (res.data)[i][j] = sepia_per_pixel(&(image->data)[i][j]);
        }
    }
    return res;
}
