#include "../include/asm-sepia.h"
#include "stdlib.h"

extern void asm_sepia_per_pixel(float arr[], struct pixel *res);

static uint8_t cut(uint64_t n){
    return n < 256 ? n : 255;
}

struct image apply_asm_sepia(const struct image *image){
    static float byte_to_float[256];
    for (size_t i = 0; i < 256; i++){
        byte_to_float[i] = i;
    }
    struct image res = {
        .height = image->height, 
        .width = image->width, 
        .data = NULL};
    create_image_data(&res);
    float array[4];
    for (size_t i = 0; i < image->height; i++){
        for (size_t j = 0; j < image->width; j++){
            array[0] = byte_to_float[image->data[i][j].r];
            array[1] = byte_to_float[image->data[i][j].g];
            array[2] = byte_to_float[image->data[i][j].b];
            asm_sepia_per_pixel(array, &res.data[i][j]);
        }
    }
    return res;
}
