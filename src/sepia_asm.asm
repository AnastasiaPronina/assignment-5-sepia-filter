%define float_size 4

section .rodata
red: dd 0.272, 0.349, 0.393, 0
green: dd 0.543, 0.686, 0.769, 0
blue: dd 0.131, 0.168, 0.189, 0

section .text
global asm_sepia_per_pixel

asm_sepia_per_pixel:
    mov al, byte[rsi + 3]

    movups xmm1, [rel red]
    movups xmm3, [rel green]
    movups xmm5, [rel blue]

    movss xmm0, [rdi]
    shufps xmm0, xmm0, 0
    movss xmm2, [rdi + float_size]
    shufps xmm2, xmm2, 0
    movss xmm4, [rdi + 2 * float_size]
    shufps xmm4, xmm4, 0

    mulps xmm0, xmm1
    mulps xmm2, xmm3
    mulps xmm4, xmm5

    addps xmm0, xmm2
    addps xmm0, xmm4

    cvtps2dq xmm0, xmm0
    packusdw xmm0, xmm0
    packuswb xmm0, xmm0

    movd [rsi], xmm0
    mov byte[rsi + 3], al
    ret
